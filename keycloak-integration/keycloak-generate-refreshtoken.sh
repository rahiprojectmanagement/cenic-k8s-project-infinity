#!/bin/bash

DEFAULT_CLUSTER_ENV=dev
VALID_ENV=( dev staging prod)
DEFAULT_KUBECONFIG_FILE=$HOME/.kube/config


#keycloak instance

validate_env() { 

    local  env=$1
    local  flag=1

    for i in ${VALID_ENV[@]}; 
        do 
            if [ "$env" == "$i" ]; then
                return 0
            fi
        done
    return 1
}


#Read the cluster environment
while true
    do
        read -p "Cluster Environment [default=$DEFAULT_CLUSTER_ENV] :" CLUSTER_ENV
        : ${CLUSTER_ENV:=$DEFAULT_CLUSTER_ENV}
        if ! validate_env $CLUSTER_ENV; 
        then
            echo "Error: $CLUSTER_ENV Invalid Environment"
        else
            break
        fi
    done


#setup Keycloak configuration
case "$CLUSTER_ENV" in
   "dev") KEYCLOAK_URL=https://keycloak-dev.mosaic.cenic.org
    KEYCLOAK_REALM=k8s
    KEYCLOAK_CLIENT_ID=kubernetes
    KEYCLOAK_CLIENT_SECRET=89d3b902-8e50-4551-a53f-04c3597afb3f
   ;;
   "staging") KEYCLOAK_URL=https://keycloak-dev.mosaic.cenic.org
    KEYCLOAK_REALM=k8s
    KEYCLOAK_CLIENT_ID=kubernetes
    KEYCLOAK_CLIENT_SECRET=89d3b902-8e50-4551-a53f-04c3597afb3f1
   ;;
   "prod") KEYCLOAK_URL=https://keycloak-dev.mosaic.cenic.org
    KEYCLOAK_REALM=k8s
    KEYCLOAK_CLIENT_ID=kubernetes
    KEYCLOAK_CLIENT_SECRET=89d3b902-8e50-4551-a53f-04c3597afb3f2
   ;;
   *) KEYCLOAK_URL=https://keycloak-dev.mosaic.cenic.org
    KEYCLOAK_REALM=k8s
    KEYCLOAK_CLIENT_ID=kubernetes
    KEYCLOAK_CLIENT_SECRET=89d3b902-8e50-4551-a53f-04c3597afb3f
   ;;
esac


#Set Kubeconfig file path
while true
    do
        read -p "Kubeconfig file path [default=$DEFAULT_KUBECONFIG_FILE] :" KUBECONFIG_FILE
        : ${KUBECONFIG_FILE:=$DEFAULT_KUBECONFIG_FILE}
        if [ ! -f "$KUBECONFIG_FILE" ]; 
        then
            echo "Error: $KUBECONFIG_FILE does not exist. Please enter valid file path"
        else
            break
        fi
    done

#read keyclock username
while true
    do
        read -p "Enter Keycloak username: for ${CLUSTER_DEV} environment:" KEYCLOAK_USERNAME
        if [ -z "$KEYCLOAK_USERNAME" ]
        then
            echo "Error: $KEYCLOAK_USERNAME Invalid Input"
        else
            break
        fi
    done

#Set Keycloak Password
while true
    do
        read -sp "Enter Keycloak Password ${CLUSTER_DEV} environment:" KEYCLOAK_PASSWORD
        if [ -z "$KEYCLOAK_PASSWORD" ]
        then
            echo "Error: $KEYCLOAK_PASSWORD Invalid Input"
        else
            break
        fi
    done



#set the KEYCLOAK configuration based on the environment

KEYCLOAK_TOKEN_URL=${KEYCLOAK_URL}/auth/realms/${KEYCLOAK_REALM}/protocol/openid-connect/token 

echo
echo "# Getting a token ... "


TOKEN=`curl --insecure -s ${KEYCLOAK_TOKEN_URL} \
  -d grant_type=password \
  -d response_type=id_token \
  -d scope=openid \
  -d client_id=${KEYCLOAK_CLIENT_ID} \
  -d client_secret=${KEYCLOAK_CLIENT_SECRET} \
  -d username=${KEYCLOAK_USERNAME} \
  -d password=${KEYCLOAK_PASSWORD}`

RET=$?
if [ "$RET" != "0" ];then
	echo "# Error ($RET) ==> ${TOKEN}";
	exit ${RET}
fi

ERROR=`echo ${TOKEN} | jq .error -r`
if [ "${ERROR}" != "null" ];then
	echo "# Failed ==> ${TOKEN}" >&2
	exit 1
fi

ID_TOKEN=`echo ${TOKEN} | jq .id_token -r`
REFRESH_TOKEN=`echo ${TOKEN} | jq .refresh_token -r`

echo "updating the Kubeconfig"

# echo "
# - name: oidc
#   user:
#     auth-provider:
#       name: oidc
#       config:
#         client-id: ${KEYCLOAK_CLIENT_ID}
#         client-secret: ${KEYCLOAK_CLIENT_SECRET}
#         idp-issuer-url: ${KEYCLOAK_URL}/auth/realms/${KEYCLOAK_REALM}
#         id-token: ${ID_TOKEN}
#         refresh-token: ${REFRESH_TOKEN}
# " >> $HOME/.kube/config


#set the kubeonfig file
kubectl config set-credentials oidc-refresh \
        --kubeconfig=${KUBECONFIG_FILE} \
	    --auth-provider=oidc  \
        --auth-provider-arg=idp-issuer-url=${KEYCLOAK_URL}/auth/realms/${KEYCLOAK_REALM} \
        --auth-provider-arg=client-id=${KEYCLOAK_CLIENT_ID} \
        --auth-provider-arg=client-secret=${KEYCLOAK_CLIENT_SECRET} \
        --auth-provider-arg=refresh-token=${REFRESH_TOKEN} \
        --auth-provider-arg=id-token=${ID_TOKEN} \
	    --auth-provider-arg=extra-scope=preferred_username


echo "...setting the context"
echo "#if you want to use this user in your context, then Run the following command"
echo "# kubectl config set-context oidc-refresh --user oidc-refresh && kubectl config use-context oidc-refresh"
echo "Operation Completed"



