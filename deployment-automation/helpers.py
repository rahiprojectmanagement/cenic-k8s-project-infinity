import ipaddress
import messages
import os
import json



#Validate Given IP Address
def validate_IP(ip):
    try: 
        if(ipaddress.ip_address(ip)):
            return True
        else:
            print(messages.INVALID_IP_ADDRESS)
            return False
    except ValueError:
        print(messages.INVALID_IP_ADDRESS)
        return False



#Validate the Subnet CIDR block
def validate_CIDR(vpcCIDRBlock):
    
    try:
        if "/" in vpcCIDRBlock:
            vpcip,vpcBits = vpcCIDRBlock.split('/')

            if( 
                ipaddress.ip_network(vpcCIDRBlock)
            ):
                return True
            else:
                print(messages.INVALID_CIDR_BLOCK)
                return False
        else:
            print(messages.INVALID_CIDR_BLOCK)
            return False
    except (ValueError, TypeError) as e:
        print(messages.INVALID_CIDR_BLOCK_CUSTOM.format(str(e)))
        return False




BACKEND_CONFIG = "server  master{0} {1}:{2} check fall 3 rise 2\n"


def GET_HA_PROXY_BACKEND_CONFIG(IPS,KUBEAPI_SERVER_PORT):
    '''
        Generate the Configuration for each master in the IPs list on the API Server PORT
        server  master1 10.32.128.160:80 check fall 3 rise 2
        server  master2 10.32.128.168:80 check fall 3 rise 2
    '''
    text = ''

    #ADD Entry for each master in the list in the above format
    for n in range(len(IPS)):
        text += BACKEND_CONFIG.format(n+1,IPS[n],KUBEAPI_SERVER_PORT)

    return text

def generateFirewallConfig(KUBEAPI_SERVER_PORT,HA_PROXY_PORT,ETCD_PEER_PORT,
                    ETCD_CLIENT_PORT,FIREWALL_PORTS_TCP,FIREWALL_PORTS_UDP):
    
    PORTS_LIST_TCP = [ KUBEAPI_SERVER_PORT,HA_PROXY_PORT,ETCD_PEER_PORT,
                    ETCD_CLIENT_PORT ] + FIREWALL_PORTS_TCP
    
    FIREWALL_CMD = "sudo firewall-cmd --permanent --zone=k8s_internal --add-port={0}/{1}\n"
    

    text = ''

    #ADD Entry for each master in the list in the above format
    for n in range(len(PORTS_LIST_TCP)):
        text += FIREWALL_CMD.format(str(PORTS_LIST_TCP[n]),'tcp')
             
    for n in range(len(FIREWALL_PORTS_UDP)):
        text += FIREWALL_CMD.format(str(FIREWALL_PORTS_UDP[n]),'udp')

    return text


def generateHAProxyInstallScript(CWD,KUBEAPI_SERVER_PORT,HA_PROXY_PORT,ETCD_PEER_PORT,
                    ETCD_CLIENT_PORT,INTERNAL_IP_RANGE,FIREWALL_PORTS_TCP,FIREWALL_PORTS_UDP,CALICO_INTERFACE_ID,EXTERNAL_IP_RANGE):
    
    #Read the default HA Proxy Config File
    haproxy_install = open(CWD + "/config_files/haproxy/" + "haproxy-install.sh","r+")
    temp_haproxy_install = haproxy_install.read()

    FIREWALL_CONFIG = generateFirewallConfig(KUBEAPI_SERVER_PORT,HA_PROXY_PORT,ETCD_PEER_PORT,
                    ETCD_CLIENT_PORT,FIREWALL_PORTS_TCP,FIREWALL_PORTS_UDP)


    #Set the configuration of the HAProxy
    # temp_haproxy_install = temp_haproxy_install.replace('<HA_PROXY_PORT>', str(HA_PROXY_PORT))
    # temp_haproxy_install = temp_haproxy_install.replace('<KUBEAPI_SERVER_PORT>', str(KUBEAPI_SERVER_PORT))
    # temp_haproxy_install = temp_haproxy_install.replace('<ETCD_CLIENT_PORT>', str(ETCD_CLIENT_PORT))
    # temp_haproxy_install = temp_haproxy_install.replace('<ETCD_PEER_PORT>', str(ETCD_PEER_PORT))
    temp_haproxy_install = temp_haproxy_install.replace('<CALICO_INTERFACE_ID>', CALICO_INTERFACE_ID)
    temp_haproxy_install = temp_haproxy_install.replace('<INTERNAL_IP_RANGE>', INTERNAL_IP_RANGE)
    temp_haproxy_install = temp_haproxy_install.replace('<EXTERNAL_IP_RANGE>', EXTERNAL_IP_RANGE)
    temp_haproxy_install = temp_haproxy_install.replace('<FIREWALL_CONFIG>', FIREWALL_CONFIG)


    #Close the file opened
    haproxy_install.close()
    return temp_haproxy_install





def generateHAProxyConfigFile(CWD,IPS,KUBEAPI_SERVER_PORT,HA_PROXY_PORT,HA_PROXY_MODE):

    #Read the default HA Proxy Config File
    haproxy_config = open(CWD + "/config_files/haproxy/" + "haproxy.cfg","r+")
    temp_haproxy_config = haproxy_config.read()

    #Generate the HA Proxy Kubernetes Cluster Configuration
    KUBERNETES_MASTER_CLUSTER_CONFIG = GET_HA_PROXY_BACKEND_CONFIG(IPS,KUBEAPI_SERVER_PORT)

    #Set the configuration of the HAProxy
    temp_haproxy_config = temp_haproxy_config.replace('<HA_PROXY_PORT>', str(HA_PROXY_PORT))
    temp_haproxy_config = temp_haproxy_config.replace('<HA_PROXY_MODE>', HA_PROXY_MODE)
    temp_haproxy_config = temp_haproxy_config.replace('<KUBERNETES_MASTER_CLUSTER_CONFIG>', KUBERNETES_MASTER_CLUSTER_CONFIG)

    #Close the file opened
    haproxy_config.close()
    return temp_haproxy_config


def make_executable(path):
    mode = os.stat(path).st_mode
    mode |= (mode & 0o444) >> 2    # copy R bits to X
    os.chmod(path, mode)



#Create a new file if doesnt exist and write the contents to it
def writeConfigFile(path,filename,contents):
    f = open(path + "/" + filename, "w")
    f.write(contents)

    #Make Executable for the sh scripts
    if(filename.find('.sh') != -1):
        make_executable(path + "/" + filename)

    f.close()
    return 





#Load a file and return the contents of it

def loadFile(path):
    f = open(path, "r+")

    if(path.find('deploy_config.json') != -1):
        contents = json.load(f)
    else:
        contents = f.read()


    f.close()
    return contents

#Generate the PeerIPs config for each node
def generateKeepAlivePeerIPs(IPS,n):
    IP_FORMAT = "        {0}"
    text = ''

    #ADD Entry for each peer in the list in the above format
    for i in range(len(IPS)):
        if(i != n):
            text += IP_FORMAT.format(IPS[i])
            
        if(i != len(IPS) - 1 and i != n ):
            text += '\n'
    return text

    
def getKeepAliveState(n):
    if(n == 0):
        return 'MASTER'
    else:
        return 'BACKUP'



#Generate the Keep alive config file
def generateKeepAlivedConfigFile(CWD,IPS,IPS_PUBLIC,n,VIRTUAL_IP_ADDRESS,NODE_INTERFACE_ID,VIRTUAL_IP_ADDRESS_PUBLIC,
    NODE_INTERFACE_ID_PUBLIC,KEEPALIVED_ACTIVE_PASSIVE):
    #Read the default HA Proxy Config File

    if(KEEPALIVED_ACTIVE_PASSIVE):
        keepalived_config = open(CWD + "/config_files/haproxy/" + "keepalived.conf","r+")
    else:
        keepalived_config = open(CWD + "/config_files/haproxy/" + "keepalived-active-active.conf","r+")



    temp_keepalived_config = keepalived_config.read()

    '''Generate Based on the Node - 
        PRIMARY_IP
        PEER_IPS
        <PRIORITY>
    '''
    PRIMARY_IP = IPS[n]
    PRIMARY_IP_PUBLIC = IPS_PUBLIC[n]

    #Get the Rest of the peer IPS
    PEER_IPS = generateKeepAlivePeerIPs(IPS,n)
    PEER_IPS_PUBLIC = generateKeepAlivePeerIPs(IPS_PUBLIC,n)
    PRIORITY = 100 + len(IPS) - n
    STATE = getKeepAliveState(n)

    #Get the virtual Public IP check if it is array 
    #if array get the IP at the index else get the string
    if(type(VIRTUAL_IP_ADDRESS) == str):
        IP_ADDRESS = VIRTUAL_IP_ADDRESS
    else:
        IP_ADDRESS = VIRTUAL_IP_ADDRESS[n]


    if(type(VIRTUAL_IP_ADDRESS_PUBLIC) == str):
        IP_ADDRESS_PUBLIC = VIRTUAL_IP_ADDRESS_PUBLIC
    else:
        IP_ADDRESS_PUBLIC = VIRTUAL_IP_ADDRESS_PUBLIC[n]



    #Set the configuration of the keepalived
    temp_keepalived_config = temp_keepalived_config.replace('<PRIMARY_IP>', str(PRIMARY_IP))
    temp_keepalived_config = temp_keepalived_config.replace('<PEER_IPS>', PEER_IPS)
    temp_keepalived_config = temp_keepalived_config.replace('<VIRTUAL_IP_ADDRESS>', "        " + IP_ADDRESS)
    temp_keepalived_config = temp_keepalived_config.replace('<PRIORITY>', str(PRIORITY))
    temp_keepalived_config = temp_keepalived_config.replace('<STATE>', STATE)
    temp_keepalived_config = temp_keepalived_config.replace('<INTERFACE_ID>', NODE_INTERFACE_ID)

    temp_keepalived_config = temp_keepalived_config.replace('<INTERFACE_ID_PUBLIC>', NODE_INTERFACE_ID_PUBLIC)
    temp_keepalived_config = temp_keepalived_config.replace('<VIRTUAL_IP_ADDRESS_PUBLIC>', "        " + IP_ADDRESS_PUBLIC)


    if(KEEPALIVED_ACTIVE_PASSIVE):
        temp_keepalived_config = temp_keepalived_config.replace('<PRIMARY_IP_PUBLIC>', str(PRIMARY_IP_PUBLIC))
        temp_keepalived_config = temp_keepalived_config.replace('<PEER_IPS_PUBLIC>', PEER_IPS_PUBLIC)

    return temp_keepalived_config





#Generate ETCD Config File By replacing the required URLS
'''
    Sample Format
    PEER_URL - https://10.32.128.160:2380
    CLIENT_URL - https://10.32.128.160:2379
    CLUSTER_URL - 10.32.128.160=https://10.32.128.160:2380,10.32.128.168=https://10.32.128.168:2380
    ETCD_LOCAL_CLIENT_URL - http://127.0.0.1:2379
    PRIMARY_IP - IP address of the Node - 10.32.128.160
'''


def generateClusterURL(IPS,ETCD_PEER_PORT,PROTOCOL):
    CLUSTER_URL_FORMAT = "{0}={2}://{0}:{1}"
    text = ''

    for i in range(len(IPS)):
        text += CLUSTER_URL_FORMAT.format(IPS[i],ETCD_PEER_PORT,PROTOCOL)
        if(i != len(IPS) - 1):
             text += ','

    return text


def generateETCDConfigFile(CWD,MASTER_IPS,n,ETCD_PEER_PORT,ETCD_CLIENT_PORT,PROTOCOL):

    #Read the default ETCD Config File
    etcd_config = open(CWD + "/config_files/k8s/" + "etcd.service","r+")
    temp_etcd_config = etcd_config.read()

    NODE_IP = MASTER_IPS[n]
    URL_FORMAT = "{2}://{0}:{1}"


    PRIMARY_IP = NODE_IP
    PEER_URL = URL_FORMAT.format(NODE_IP,ETCD_PEER_PORT,PROTOCOL)
    CLIENT_URL = URL_FORMAT.format(NODE_IP,ETCD_CLIENT_PORT,PROTOCOL) 
    ETCD_LOCAL_CLIENT_URL = URL_FORMAT.format('127.0.0.1',ETCD_CLIENT_PORT,PROTOCOL) 
    CLUSTER_URL = generateClusterURL(MASTER_IPS,ETCD_PEER_PORT,PROTOCOL)


    #Set the configuration of the keepalived
    temp_etcd_config = temp_etcd_config.replace('<PRIMARY_IP>', str(PRIMARY_IP))
    temp_etcd_config = temp_etcd_config.replace('<PEER_URL>', PEER_URL)
    temp_etcd_config = temp_etcd_config.replace('<CLIENT_URL>', CLIENT_URL)
    temp_etcd_config = temp_etcd_config.replace('<ETCD_LOCAL_CLIENT_URL>', ETCD_LOCAL_CLIENT_URL)
    temp_etcd_config = temp_etcd_config.replace('<CLUSTER_URL>', CLUSTER_URL)

    return temp_etcd_config





#Generate Kubernetes config.yaml file
'''
    Sample Format
    <VIRTUAL_IP_ADDRESS> - 
    - 10.32.128.173
    <CONTROL_PLANE_ENDPOINT> - "10.32.128.173:6444"
    <ENDPOINTS> -     
    - https://10.32.128.160:2379
    - https://10.32.128.168:2379
    <POD_SUBNET_CIDR> - 192.168.0.0/16
    <NUM_MASTERS> - 3
'''


def generateK8sConfigEndpoints(IPS,ETCD_CLIENT_PORT,PROTOCOL):

    ENDPOINT_URL_FORMAT = "    - {2}://{0}:{1}"

    text = ''

    for i in range(len(IPS)):
        text += ENDPOINT_URL_FORMAT.format(IPS[i],ETCD_CLIENT_PORT,PROTOCOL)
        if(i != len(IPS) - 1):
             text += '\n'

    return text


def generateK8sCertSANS(MASTER_IPS,MASTER_IPS_PUBLIC,VIRTUAL_IP_ADDRESS,DNS_SERVER,VIRTUAL_IP_ADDRESS_PUBLIC):

    IP_LIST = MASTER_IPS + MASTER_IPS_PUBLIC + [ DNS_SERVER ]

    if(type(VIRTUAL_IP_ADDRESS_PUBLIC) == str):
        IP_LIST = IP_LIST + [  VIRTUAL_IP_ADDRESS_PUBLIC ]
    else:
        IP_LIST = IP_LIST + VIRTUAL_IP_ADDRESS_PUBLIC


    if(type(VIRTUAL_IP_ADDRESS) == str):
        IP_LIST = IP_LIST + [  VIRTUAL_IP_ADDRESS ]
    else:
        IP_LIST = IP_LIST + VIRTUAL_IP_ADDRESS



    FORMAT = '  - "{0}"'

    text = ''

    for i in range(len(IP_LIST)):
        text += FORMAT.format(IP_LIST[i])
        if(i != len(IP_LIST) - 1):
             text += '\n'
    return text




#generate the K8s config file
def generateK8sConfigFile(CWD,MASTER_IPS,MASTER_IPS_PUBLIC,n,VIRTUAL_IP_ADDRESS,VIRTUAL_IP_ADDRESS_PUBLIC,ETCD_CLIENT_PORT,PROTOCOL,NUM_MASTERS,HA_PROXY_PORT,POD_SUBNET_CIDR,KUBERNETES_VERSION,DNS_SERVER):
    #Read the default k8s Config File
    k8s_config = open(CWD + "/config_files/k8s/" + "config.yaml","r+")
    temp_k8s_config = k8s_config.read()

    #Virtual IP address format
    CONTROL_PLANE_ENDPOINT_FORMAT = "{0}:{1}"


    NODE_IP = MASTER_IPS[n]
    ENDPOINTS = generateK8sConfigEndpoints(MASTER_IPS,ETCD_CLIENT_PORT,PROTOCOL)
    CERT_SANS = generateK8sCertSANS(MASTER_IPS,MASTER_IPS_PUBLIC,VIRTUAL_IP_ADDRESS,DNS_SERVER,VIRTUAL_IP_ADDRESS_PUBLIC)
    

    if(type(VIRTUAL_IP_ADDRESS) == str):
        IP_ADDRESS = VIRTUAL_IP_ADDRESS
    else:
        IP_ADDRESS = VIRTUAL_IP_ADDRESS[0]

    CONTROL_PLANE_ENDPOINT = CONTROL_PLANE_ENDPOINT_FORMAT.format(IP_ADDRESS,HA_PROXY_PORT)

    #Set the configuration of the keepalived
    temp_k8s_config = temp_k8s_config.replace('<VIRTUAL_IP_ADDRESS>', IP_ADDRESS)
    temp_k8s_config = temp_k8s_config.replace('<CONTROL_PLANE_ENDPOINT>', CONTROL_PLANE_ENDPOINT)
    temp_k8s_config = temp_k8s_config.replace('<ENDPOINTS>', ENDPOINTS)
    temp_k8s_config = temp_k8s_config.replace('<POD_SUBNET_CIDR>', POD_SUBNET_CIDR)
    temp_k8s_config = temp_k8s_config.replace('<NUM_MASTERS>', str(NUM_MASTERS))
    temp_k8s_config = temp_k8s_config.replace('<KUBERNETES_VERSION>', KUBERNETES_VERSION)
    temp_k8s_config = temp_k8s_config.replace('<CERT_SANS>', CERT_SANS)
    

    return temp_k8s_config

#Generate the ca-csr json config file

def generateCaCsrConfigFile(CWD,C,L,O,OU,ST):
    #Read the default k8s Config File
    ca_csr_config = open(CWD + "/config_files/k8s/certs/" + "ca-csr.json","r+")
    temp_ca_csr_config = ca_csr_config.read()


    #Set the configuration of the kubernetes-csr config files
    temp_ca_csr_config = temp_ca_csr_config.replace('<CERT_NAME_C>', C)
    temp_ca_csr_config = temp_ca_csr_config.replace('<CERT_NAME_L>', L)
    temp_ca_csr_config = temp_ca_csr_config.replace('<CERT_NAME_O>', O)
    temp_ca_csr_config = temp_ca_csr_config.replace('<CERT_NAME_OU>', OU)
    temp_ca_csr_config = temp_ca_csr_config.replace('<CERT_NAME_ST>', ST)

    return temp_ca_csr_config


def generateKubernetesCsrHostsFormat(IPS):
    HOST_FORMAT = '        "{0}",'

    text = ''

    for i in range(len(IPS)):
        text += HOST_FORMAT.format(IPS[i])
        if(i != len(IPS) - 1):
             text += '\n'

    return text



#Generate the Kubernetes-csr-config file 
def generateKubernetesCsrConfigFile(CWD,MASTER_IPS,MASTER_IPS_PUBLIC,VIRTUAL_IP_ADDRESS,C,L,O,OU,ST):
    #Read the default kubereneste-csr Config File
    kubernetes_csr_config = open(CWD + "/config_files/k8s/certs/" + "kubernetes-csr.json","r+")
    temp_kubernetes_csr_config = kubernetes_csr_config.read()

    HOSTS = generateKubernetesCsrHostsFormat(MASTER_IPS + MASTER_IPS_PUBLIC)


    #Set the <MASTER_NODE_IPS> 
    temp_kubernetes_csr_config = temp_kubernetes_csr_config.replace('<MASTER_NODE_IPS>', HOSTS)

    #Set the configuration of the kubernetes-csr config files
    temp_kubernetes_csr_config = temp_kubernetes_csr_config.replace('<CERT_NAME_C>', C)
    temp_kubernetes_csr_config = temp_kubernetes_csr_config.replace('<CERT_NAME_L>', L)
    temp_kubernetes_csr_config = temp_kubernetes_csr_config.replace('<CERT_NAME_O>', O)
    temp_kubernetes_csr_config = temp_kubernetes_csr_config.replace('<CERT_NAME_OU>', OU)
    temp_kubernetes_csr_config = temp_kubernetes_csr_config.replace('<CERT_NAME_ST>', ST)

    return temp_kubernetes_csr_config






#Generate the Join the worker cluster node

def generateClusterJoinScript(CWD,VIRTUAL_IP_ADDRESS,HA_PROXY_PORT,MODE):

    #Read the default join-worker
    if(MODE == "MASTER"):
        file_name = "join-cluster.sh"
    elif(MODE == "WORKER"):
        file_name = "join-cluster-worker.sh"


    join_cluster_config = open(CWD + "/config_files/k8s/" + file_name,"r+")
    temp_join_cluster_config = join_cluster_config.read()


    VIP_URL_FORMAT = "{0}:{1}"

    VIP_URL = VIP_URL_FORMAT.format(VIRTUAL_IP_ADDRESS,HA_PROXY_PORT)


    #Set the configuration of the join-worker
    temp_join_cluster_config = temp_join_cluster_config.replace('<VIP_URL>', VIP_URL)
    

    return temp_join_cluster_config


def generateFirewallConfigWorker(KUBEAPI_SERVER_PORT,HA_PROXY_PORT,FIREWALL_PORTS_TCP_WORKER,FIREWALL_PORTS_UDP_WORKER):
    
    PORTS_LIST_TCP = [ KUBEAPI_SERVER_PORT,HA_PROXY_PORT ] + FIREWALL_PORTS_TCP_WORKER
    
    FIREWALL_CMD = "sudo firewall-cmd --permanent --zone=k8s_internal --add-port={0}/{1}\n"
    

    text = ''

    #ADD Entry for each master in the list in the above format
    for n in range(len(PORTS_LIST_TCP)):
        text += FIREWALL_CMD.format(str(PORTS_LIST_TCP[n]),'tcp')
             
    for n in range(len(FIREWALL_PORTS_UDP_WORKER)):
        text += FIREWALL_CMD.format(str(FIREWALL_PORTS_UDP_WORKER[n]),'udp')

    return text


def generateWorkerK8Script(CWD,HA_PROXY_PORT,INTERNAL_IP_RANGE,KUBEAPI_SERVER_PORT,KUBEADM_VERSION,KUBECTL_VERSION,KUBELET_VERSION,
        DOCKER_VERSION,FIREWALL_PORTS_TCP_WORKER,FIREWALL_PORTS_UDP_WORKER):

    k8s_install_worker = open(CWD + "/config_files/k8s/" + "k8s-install_worker.sh","r+")
    temp_k8s_install_worker = k8s_install_worker.read()

    FIREWALL_CONFIG = generateFirewallConfigWorker(KUBEAPI_SERVER_PORT,HA_PROXY_PORT,FIREWALL_PORTS_TCP_WORKER,FIREWALL_PORTS_UDP_WORKER)


    #Set the configuration of the HAProxy
    temp_k8s_install_worker = temp_k8s_install_worker.replace('<HA_PROXY_PORT>', str(HA_PROXY_PORT))
    temp_k8s_install_worker = temp_k8s_install_worker.replace('<KUBEAPI_SERVER_PORT>', str(KUBEAPI_SERVER_PORT))
    temp_k8s_install_worker = temp_k8s_install_worker.replace('<INTERNAL_IP_RANGE>', INTERNAL_IP_RANGE)
    temp_k8s_install_worker = temp_k8s_install_worker.replace('<KUBEADM_VERSION>', KUBEADM_VERSION)
    temp_k8s_install_worker = temp_k8s_install_worker.replace('<KUBECTL_VERSION>', KUBECTL_VERSION)
    temp_k8s_install_worker = temp_k8s_install_worker.replace('<KUBELET_VERSION>', KUBELET_VERSION)
    temp_k8s_install_worker = temp_k8s_install_worker.replace('<DOCKER_VERSION>', DOCKER_VERSION)
    temp_k8s_install_worker = temp_k8s_install_worker.replace('<FIREWALL_CONFIG>', FIREWALL_CONFIG)


    #Close the file opened
    k8s_install_worker.close()
    return temp_k8s_install_worker


def generateK8sInstallScript(CWD,ETCD_VERSION,KUBEADM_VERSION,KUBECTL_VERSION,KUBELET_VERSION,DOCKER_VERSION):
    k8s_install = open(CWD + "/config_files/k8s/" + "k8s-install.sh","r+")
    temp_k8s_install = k8s_install.read()


    #Set the configuration of the install versions
    temp_k8s_install = temp_k8s_install.replace('<ETCD_VERSION>', ETCD_VERSION)
    temp_k8s_install = temp_k8s_install.replace('<KUBEADM_VERSION>', KUBEADM_VERSION)
    temp_k8s_install = temp_k8s_install.replace('<KUBECTL_VERSION>', KUBECTL_VERSION)
    temp_k8s_install = temp_k8s_install.replace('<KUBELET_VERSION>', KUBELET_VERSION)
    temp_k8s_install = temp_k8s_install.replace('<DOCKER_VERSION>', DOCKER_VERSION)


    #Close the file opened
    k8s_install.close()
    return temp_k8s_install



#Generate calico interface file

def generateCalicoConfigFile(CWD,CALICO_INTERFACE_ID):
    calico_config = open(CWD + "/config_files/k8s/" + "calico.yaml","r+")
    temp_calico_config = calico_config.read()


    #Set the configuration of the install versions
    temp_calico_config = temp_calico_config.replace('<CALICO_INTERFACE>', CALICO_INTERFACE_ID)


    #Close the file opened
    calico_config.close()
    return temp_calico_config




#Get delta in the configuration and ask for the confirmation to overwrite
def getDeltaConfig(CONFIG, CURRENT_CONFIG):

    conflict = False
    
    if(CONFIG and type(CONFIG) == dict):
        for key in CONFIG.keys(): 
            if(CURRENT_CONFIG and not key in CURRENT_CONFIG ):

                print("... change in configuration {0} from {1} to {2}".format(key,None,CONFIG[key]))
                conflict = True
                
            elif(CURRENT_CONFIG and not (
                    (key in CURRENT_CONFIG) and CONFIG[key] == CURRENT_CONFIG[key]
                )
            ):
                print("... change in configuration {0} from {1} to {2}".format(key,CURRENT_CONFIG[key],CONFIG[key]))
                conflict = True
    return conflict





def generateKubeadmReloadFile(CWD,IPS,n):

    kubeadm_reload = open(CWD + "/config_files/k8s/" + "reload_kubeadm.sh","r+")
    temp_kubeadm_reload = kubeadm_reload.read()

    NODE_IP = IPS[n]
    temp_kubeadm_reload = temp_kubeadm_reload.replace('<NODE_IP>', NODE_IP)

    return temp_kubeadm_reload


#Generate the reload kubeadm worker file from the template
def generateKubeadmReloadWorkerFile(CWD,INTERFACE_ID):
    kubeadm_reload = open(CWD + "/config_files/k8s/" + "reload_kubeadm_worker.sh","r+")
    temp_kubeadm_reload = kubeadm_reload.read()

    temp_kubeadm_reload = temp_kubeadm_reload.replace('<INTERFACE_ID>', INTERFACE_ID)

    return temp_kubeadm_reload