import messages
import ipaddress
from helpers import *
import os
import sys, getopt
from os import path
import json
import shutil 


import calendar;
import time;
timestamp = calendar.timegm(time.gmtime())


#GET the No of masters and No of workers 
DEFAULT_NUM_MASTERS = 3
DEFAULT_NUM_WORKERS = 3
DEFAULT_KUBEAPI_SERVER_PORT = 6443
DEFAULT_PROTOCOL = 'https'


#HA proxy Default Config
# List of variables required for the HA proxy Config
VALID_HAPROXY_MODES = ['tcp', 'http']
DEFAULT_HA_PROXY_MODE = 'tcp'
DEFAULT_HA_PROXY_PORT = '6445'


#Get the list of IPs based on the Number of nodes
MASTER_IPS = []
WORKER_IPS = []
MASTER_IPS_PUBLIC = []



#ETCD Configuration
DEFAULT_ETCD_PEER_PORT = 2380
DEFAULT_ETCD_CLIENT_PORT = 2379
DEFAULT_ETCD_VERSION = "v3.4.10"
DEFAULT_DOCKER_VERSION = "v3.4.10"
DEFAULT_KUBECTL_VERSION = "v3.4.10"
DEFAULT_KUBEADM_VERSION = "v3.4.10"
DEFAULT_KUBELET_VERSION = "v3.4.10"

DEFAULT_KEEPALIVED_ACTIVE_PASSIVE = True




#Default CERT Configuration
DEFAULT_CERT_NAME_C = "US"
DEFAULT_CERT_NAME_L = "La Mirada"
DEFAULT_CERT_NAME_O = "CENIC"
DEFAULT_CERT_NAME_OU = "Systems"
DEFAULT_CERT_NAME_ST = "California"

NO_RESPONSE = ['n', 'No', 'NO', 'N', 'no']
YES_RESPONSE = ['yes', 'YES', 'y', 'Yes', 'ye', 'Ye', 'YE']



def main(argv):

    CONFIG = None
    MASTER_IPS_PUBLIC = []
    MASTER_IPS = []

    INPUT_FILE = None
    try:
        opts, args = getopt.getopt(argv,"hf:",["ffile="])
    except getopt.GetoptError:
        print('deploy.py -f <inputfile>')
        sys.exit(2)


    for opt, arg in opts:
        if opt == '-h':
            print('deploy.py -f <inputfile>')
            sys.exit()
        elif opt in ("-f", "--ffile"):
            INPUT_FILE = arg
    print(' Reading Configuration from File :', INPUT_FILE)


    # GET the current working directory
    CWD = os.getcwd()


    if(INPUT_FILE):

        #Validate if the file Exists
        if( path.exists(INPUT_FILE) or path.exists("{}/{}".format(CWD,INPUT_FILE)) ):
            FROM_CONFIG_FILE = True
        else:
            FROM_CONFIG_FILE = False
            print("Error: File does not exist. Please enter valid file path")
            sys.exit()

        #Read the Config file
        if(path.exists(INPUT_FILE)):
            f = open(INPUT_FILE) 
        elif(path.exists("{}/{}".format(CWD,INPUT_FILE))):
            f = open(INPUT_FILE) 

        CONFIG = json.load(f)

    


    # #**********************************************************************************
    # #               Setup Cluster Environment
    # #**********************************************************************************

    #prompt for the deployment environment
    if(CONFIG 
        and "environment" in CONFIG
        and CONFIG["environment"]):
        DEPLOYMENT_ENVIRONMENT = CONFIG["environment"]
    else:
        while True:
            DEPLOYMENT_ENVIRONMENT = input(messages.PROMPT_DEPLOYMENT_ENVIRONMENT)

            if(DEPLOYMENT_ENVIRONMENT):
                break





    # #**********************************************************************************
    # #               Setup Master and Worker Node Information
    # #**********************************************************************************

    def validateIPList(IPs):
        isValid = True
        for n in range(len(IPs)):
            isValid = validate_IP(IPs[n])
            if(not isValid):
                return isValid
        return isValid
            


    if(CONFIG 
        and "master_nodes" in CONFIG
        and "private_ips" in CONFIG["master_nodes"]
        and CONFIG["master_nodes"]["private_ips"]
        and len(CONFIG["master_nodes"]["private_ips"]) > 0
        and validateIPList(CONFIG["master_nodes"]["private_ips"])
    ):
        MASTER_IPS = CONFIG["master_nodes"]["private_ips"]
        NUM_MASTERS = len(CONFIG["master_nodes"]["private_ips"])
    else:
        while True:
            try:
                #prompt for the Number of Master Nodes
                NUM_MASTERS = int(input(messages.PROMPT_NO_OF_MASTER_NODES.format(DEFAULT_NUM_MASTERS)) or DEFAULT_NUM_MASTERS)

                #validate the input from the User
                if( NUM_MASTERS > 0 ):
                    # NUM_HA_PROXY = NUM_MASTERS
                    break

            except ValueError:
                print(messages.INVALID_NUMBER_INPUT)


        #GET the list of master and worker IP Addresses based on the No of Nodes
        for n in range(1,NUM_MASTERS + 1):
            while True:
                #prompt for the Number of Worker Nodes
                if(n == 1):
                    TEMP_MASTER_IP = input(messages.PROMPT_MASTER_IP_ROOT.format(n))
                else:
                    TEMP_MASTER_IP = input(messages.PROMPT_MASTER_IP.format(n))

                #Validate the IP Address Entered
                if(validate_IP(TEMP_MASTER_IP)):
                    MASTER_IPS.append(TEMP_MASTER_IP)
                    break




        # while True:
        #     try:
        #         #prompt for the Number of Worker Nodes
        #         NUM_WORKERS = int( input(
        #             messages.PROMPT_NO_OF_WORKER_NODES.format(DEFAULT_NUM_WORKERS) 
        #         ) or DEFAULT_NUM_WORKERS )

        #         #validate the input from the User
        #         if( NUM_WORKERS > 0):
        #             break

        #     except ValueError:
        #         print(messages.INVALID_NUMBER_INPUT)

    if(CONFIG 
        and "master_nodes" in CONFIG
        and "public_ips" in CONFIG["master_nodes"]
        and CONFIG["master_nodes"]["public_ips"]
        and len(CONFIG["master_nodes"]["public_ips"]) > 0
        and validateIPList(CONFIG["master_nodes"]["public_ips"])
    ):
        if(not (len(CONFIG["master_nodes"]["public_ips"]) == len(CONFIG["master_nodes"]["private_ips"]) ) ):
            print(messages.PROMPT_INVALID_IP_CONFIG)
            sys.exit()
        else:
            MASTER_IPS_PUBLIC = CONFIG["master_nodes"]["public_ips"]
    else:

        #GET the list of master and worker IP Addresses based on the No of Nodes
        for n in range(1,NUM_MASTERS + 1):
            while True:
                try:
                    #prompt for the Number of Worker Nodes
                    if(n == 1):
                        TEMP_MASTER_IP_PUBLIC = input(messages.PROMPT_MASTER_IP_ROOT_PUBLIC.format(n))
                    else:
                        TEMP_MASTER_IP_PUBLIC = input(messages.PROMPT_MASTER_IP_PUBLIC.format(n))

                    #Validate the IP Address Entered
                    if(validate_IP(TEMP_MASTER_IP_PUBLIC)):
                        MASTER_IPS_PUBLIC.append(TEMP_MASTER_IP_PUBLIC)
                        break
                except ValueError:
                        print(messages.INVALID_NUMBER_INPUT)
                






    # #GET the list of master and worker IP Addresses based on the No of Nodes
    # for n in range(1,NUM_WORKERS + 1):
    #     while True:
    #         #prompt for the Number of Worker Nodes
    #         TEMP_WORKER_IP = input(messages.PROMPT_WORKER_IP.format(n))

    #         #Validate the IP Address Entered
    #         if(validate_IP(TEMP_WORKER_IP)):
    #             WORKER_IPS.append(TEMP_WORKER_IP)
    #             break





    # #**********************************************************************************
    # #               Setup Keep Alive Config
    # #**********************************************************************************


    def validateVirtualIP(data):
        if(not type(data) == str):
            if(len(data) == NUM_MASTERS):
                return True
            else:
                print("Mismatch with Num of Master nodes")
                return False
        else:
            return True





    #Prompt for the Keep-Alive Config
    '''
        prompt VIP_ADDRESS
        Generate Based on the Node - 
            PRIMARY_IP
            PEER_IPS
        INTERFACE_ID - <INTERFACE_ID>
    '''
    if(CONFIG 
        and "virtual_ip" in CONFIG
        and ( validate_IP(CONFIG["virtual_ip"]) or validateIPList(CONFIG["virtual_ip"]) )
        and validateVirtualIP(CONFIG["virtual_ip"])
    ):
        VIRTUAL_IP_ADDRESS = CONFIG["virtual_ip"]

    else:
        while True:
            #prompt for the Validate the IP Address
            VIRTUAL_IP_ADDRESS = input(messages.PROMPT_VIRTUAL_IP_ADDRESS)

            #Validate the IP Address Entered
            if(validate_IP(VIRTUAL_IP_ADDRESS)):
                break


    #Prompt for the Keep-Alive Config
    '''
        prompt VIP_ADDRESS
        Generate Based on the Node - 
            PRIMARY_IP
            PEER_IPS
        INTERFACE_ID - <INTERFACE_ID>
    '''
    if(CONFIG 
        and "virtual_ip_public" in CONFIG
        and ( validate_IP(CONFIG["virtual_ip_public"]) or validateIPList(CONFIG["virtual_ip_public"]) )
        and validateVirtualIP(CONFIG["virtual_ip_public"])
    ):
        VIRTUAL_IP_ADDRESS_PUBLIC = CONFIG["virtual_ip_public"]
    else:
        while True:
            #prompt for the Validate the IP Address
            VIRTUAL_IP_ADDRESS_PUBLIC = input(messages.PROMPT_VIRTUAL_IP_ADDRESS_PUBLIC)

            #Validate the IP Address Entered
            if(validate_IP(VIRTUAL_IP_ADDRESS_PUBLIC)):
                break



    if(CONFIG 
        and "interface_id" in CONFIG
        and CONFIG["interface_id"]
    ):

        NODE_INTERFACE_ID = CONFIG["interface_id"]

    else:
        #prompt for keepalive Interface ID
        while True:
            try:
                NODE_INTERFACE_ID = input(messages.PROMPT_NODE_INTERFACE_ID)

                #validate the input from the User
                if( NODE_INTERFACE_ID ):
                    break

            except ValueError:
                print(messages.ERROR)


    if(CONFIG 
        and "interface_id_public" in CONFIG
        and CONFIG["interface_id_public"]
    ):

        NODE_INTERFACE_ID_PUBLIC = CONFIG["interface_id_public"]

    else:
        #prompt for keepalive Interface ID
        while True:
            try:
                NODE_INTERFACE_ID_PUBLIC = input(messages.PROMPT_NODE_INTERFACE_ID_PUBLIC)

                #validate the input from the User
                if( NODE_INTERFACE_ID_PUBLIC ):
                    break

            except ValueError:
                print(messages.ERROR)


    #Calico bind interface ID
    if(CONFIG 
        and "calico_interface" in CONFIG
        and CONFIG["calico_interface"]
    ):

        CALICO_INTERFACE_ID = CONFIG["calico_interface"]

    else:
        #prompt for calico bind Interface ID
        while True:
            try:
                CALICO_INTERFACE_ID = input(messages.PROMPT_CALICO_INTERFACE_ID)

                #validate the input from the User
                if( CALICO_INTERFACE_ID ):
                    break

            except ValueError:
                print(messages.ERROR)



    if(CONFIG 
        and "keepalived" in CONFIG
        and "active_passive" in CONFIG["keepalived"]
        and ( CONFIG["keepalived"]["active_passive"] == True or CONFIG["keepalived"]["active_passive"] == False )
    ):
        KEEPALIVED_ACTIVE_PASSIVE = CONFIG["keepalived"]["active_passive"]
    else:
        #Prompt for the keepalived Config
        '''
            Keepalived Active passive flag 
            If True Only one Haproxy will be active and rest will be passive 
        '''
        #Prompt for the keepalived Config
        while True:
            try:
                KEEPALIVED_ACTIVE_PASSIVE = int(input(messages.PROMPT_KEEPALIVED_ACTIVE_PASSIVE.format(DEFAULT_KEEPALIVED_ACTIVE_PASSIVE)) or DEFAULT_KEEPALIVED_ACTIVE_PASSIVE)

                #validate the input from the flag
                if(KEEPALIVED_ACTIVE_PASSIVE == True or KEEPALIVED_ACTIVE_PASSIVE == False):
                    break

            except ValueError:
                print(messages.ERROR)


    # #**********************************************************************************
    # #               Firewall Configuration
    # #**********************************************************************************

    #Prompt for the INTERNAL_IP_RANGE
    '''
        INTERNAL_IP_RANGE
    '''
    if(CONFIG 
        and "internal_ip_range" in CONFIG
        and validate_CIDR(CONFIG["internal_ip_range"])):

        INTERNAL_IP_RANGE = CONFIG["internal_ip_range"]

    else:
        while True:
            #prompt for the internal ip range for the firewall configuration
            INTERNAL_IP_RANGE = input(messages.PROMPT_INTERNAL_IP_RANGE)

            #Validate the IP Address Entered
            if(validate_CIDR(INTERNAL_IP_RANGE)):
                break


    #Prompt for the EXTERNAL_IP_RANGE
    '''
        EXTERNAL_IP_RANGE
    '''
    if(CONFIG 
        and "external_ip_range" in CONFIG
        and validate_CIDR(CONFIG["external_ip_range"])):

        EXTERNAL_IP_RANGE = CONFIG["external_ip_range"]

    else:
        while True:
            #prompt for the internal ip range for the firewall configuration
            EXTERNAL_IP_RANGE = input(messages.PROMPT_EXTERNAL_IP_RANGE)

            #Validate the IP Address Entered
            if(validate_CIDR(EXTERNAL_IP_RANGE)):
                break

        
    # #**********************************************************************************
    # #               Setup HA PROXY CONFIG
    # #**********************************************************************************



    if(CONFIG 
        and "haproxy" in CONFIG
        and "port" in CONFIG["haproxy"]
        and CONFIG["haproxy"]["port"]
        and  1 <= CONFIG["haproxy"]["port"] <= 65535 
    ):
        HA_PROXY_PORT = CONFIG["haproxy"]["port"]

    else:

        #Prompt for the HA Proxy Config

        '''
            Prompt HA_PROXY_PORT
            Prompt HA_PROXY_MODE
            Generate KUBERNETES_MASTER_CLUSTER_CONFIG #HA Proxy Backend Servers
        '''
        #prompt for HA_PROXY_PORT
        while True:
            try:
                HA_PROXY_PORT = int(input(messages.PROMPT_HA_PROXY_PORT.format(DEFAULT_HA_PROXY_PORT)) or DEFAULT_HA_PROXY_PORT)

                #validate the input from the User
                if( 1 <= HA_PROXY_PORT <= 65535 ):
                    break

            except ValueError:
                print(messages.ERROR)



    if(CONFIG 
        and "haproxy" in CONFIG
        and "mode" in CONFIG["haproxy"]
        and CONFIG["haproxy"]["mode"]
        and CONFIG["haproxy"]["mode"]in VALID_HAPROXY_MODES
    ):
        HA_PROXY_MODE = CONFIG["haproxy"]["mode"]
    else:
        #prompt for HA_PROXY_MODE
        '''
        Valid Modes 
            tcp
            http
        '''

        #prompt for HA_PROXY_MODE
        while True:
            try:
                HA_PROXY_MODE = input(messages.PROMPT_HA_PROXY_MODE.format(DEFAULT_HA_PROXY_MODE)) or DEFAULT_HA_PROXY_MODE

                #validate the input from the User
                if( HA_PROXY_MODE in VALID_HAPROXY_MODES ):
                    break

            except ValueError:
                print(messages.ERROR)




    # #**********************************************************************************
    # #                                             End
    # #**********************************************************************************




        
    # #**********************************************************************************
    # #               Setup ETCD Configuration
    # #**********************************************************************************

    if(CONFIG 
        and "etcd" in CONFIG
        and "peer_port" in CONFIG["etcd"]
        and CONFIG["etcd"]["peer_port"]
        and 1 <= CONFIG["etcd"]["peer_port"] <= 65535
    ):
        ETCD_PEER_PORT = CONFIG["etcd"]["peer_port"]
    else:
        '''
            ETCD_PEER_PORT = 2380
            ETCD_CLIENT_PORT = 2379
        '''

        #prompt for ETCD_PEER_PORT
        while True:
            try:
                ETCD_PEER_PORT = int(input(messages.PROMPT_ETCD_PEER_PORT.format(DEFAULT_ETCD_PEER_PORT)) or DEFAULT_ETCD_PEER_PORT)

                #validate the input from the User
                if( 1 <= HA_PROXY_PORT <= 65535 ):
                    break

            except ValueError:
                print(messages.ERROR)

    if(CONFIG 
        and "etcd" in CONFIG
        and "client_port" in CONFIG["etcd"]
        and CONFIG["etcd"]["client_port"]
        and 1 <= CONFIG["etcd"]["client_port"] <= 65535
    ):

        ETCD_CLIENT_PORT = CONFIG["etcd"]["client_port"]

    else:
        #prompt for ETCD_PEER_PORT
        while True:
            try:
                ETCD_CLIENT_PORT = int(input(messages.PROMPT_ETCD_CLIENT_PORT.format(DEFAULT_ETCD_CLIENT_PORT)) or DEFAULT_ETCD_CLIENT_PORT)

                #validate the input from the User
                if( 1 <= ETCD_CLIENT_PORT <= 65535 ):
                    break

            except ValueError:
                print(messages.ERROR)


            



    # #**********************************************************************************
    # #                               kubernetes cluster configuration
    # #**********************************************************************************


    #Kubernetes version
    if(CONFIG 
        and "kubernetes_version" in CONFIG
        and CONFIG["kubernetes_version"]
    ):

        KUBERNETES_VERSION = CONFIG["kubernetes_version"]

    else:
        #prompt for kubernetes version
        while True:
            try:
                KUBERNETES_VERSION = input(messages.PROMPT_KUBERENETES_VERSION)

                #validate the input from the User
                if( KUBERNETES_VERSION ):
                    break

            except ValueError:
                print(messages.ERROR)


    #DNS server
    if(CONFIG 
        and "dns_server" in CONFIG
        and CONFIG["dns_server"]
    ):

        DNS_SERVER = CONFIG["dns_server"]

    else:
        #prompt for kubernetes version
        while True:
            try:
                DNS_SERVER = input(messages.PROMPT_DNS_SERVER)

                #validate the input from the User
                if( DNS_SERVER ):
                    break

            except ValueError:
                print(messages.ERROR)


    if(CONFIG 
        and "kubectl" in CONFIG
        and "api_port" in CONFIG["kubectl"]
        and CONFIG["kubectl"]["api_port"]
        and 1 <= CONFIG["kubectl"]["api_port"] <= 65535
    ):

        KUBEAPI_SERVER_PORT = CONFIG["kubectl"]["api_port"]
    
    else:

        '''
            Networking config 
            podSubnet - <CIDR> eq : 192.168.0.0/16 
        '''

        #prompt for DEFAULT_KUBEAPI_SERVER_PORT
        while True:
            try:
                KUBEAPI_SERVER_PORT = int(input(messages.PROMPT_KUBEAPI_SERVER_PORT.format(DEFAULT_KUBEAPI_SERVER_PORT)) or DEFAULT_KUBEAPI_SERVER_PORT)

                #validate the input from the User
                if( 1 <= KUBEAPI_SERVER_PORT <= 65535 ):
                    break

            except ValueError:
                print(messages.ERROR)



    if(CONFIG 
        and "kubectl" in CONFIG
        and "pod_subnet_cidr" in CONFIG["kubectl"]
        and CONFIG["kubectl"]["pod_subnet_cidr"]
        and validate_CIDR(CONFIG["kubectl"]["pod_subnet_cidr"])
    ):

        POD_SUBNET_CIDR = CONFIG["kubectl"]["pod_subnet_cidr"]
    
    else:

        #prompt for the POD_SUBNET_CIDR
        while True:
            POD_SUBNET_CIDR = input(messages.PROMPT_POD_SUBNET_CIDR)

            #validate subnet CIDR Entered
            if(POD_SUBNET_CIDR and validate_CIDR(POD_SUBNET_CIDR)):
                break



    


        
    # #**********************************************************************************
    # #               Setup Cert Configuration
    # #**********************************************************************************
    '''
        "C": "<CERT_NAME_C>",
        "L": "<CERT_NAME_L>",
        "O": "<CERT_NAME_O>",
        "OU": "<CERT_NAME_OU>",
        "ST": "<CERT_NAME_ST>"
    '''


    if(CONFIG 
        and "cert_names" in CONFIG
        and "c" in CONFIG["cert_names"]
        and CONFIG["cert_names"]["c"]
    ):
        CERT_NAME_C = CONFIG["cert_names"]["c"]
    else:
        #Prompt for the Country Name
        while True:
            try:
                CERT_NAME_C = input(messages.PROMPT_CERT_NAME_C.format(DEFAULT_CERT_NAME_C)) or DEFAULT_CERT_NAME_C

                #validate the input from the User
                if( CERT_NAME_C):
                    break

            except ValueError:
                print(messages.ERROR)



    if(CONFIG 
        and "cert_names" in CONFIG
        and "l" in CONFIG["cert_names"]
        and CONFIG["cert_names"]["l"]
    ):
        CERT_NAME_L = CONFIG["cert_names"]["l"]
    else:
        #Prompt for the Location
        while True:
            try:
                CERT_NAME_L = input(messages.PROMPT_CERT_NAME_L.format(DEFAULT_CERT_NAME_L)) or DEFAULT_CERT_NAME_L

                #validate the input from the User
                if( CERT_NAME_L):
                    break

            except ValueError:
                print(messages.ERROR)

    if(CONFIG 
        and "cert_names" in CONFIG
        and "o" in CONFIG["cert_names"]
        and CONFIG["cert_names"]["o"]
    ):
        CERT_NAME_O = CONFIG["cert_names"]["o"]
    else:
        #Prompt for the Organization
        while True:
            try:
                CERT_NAME_O = input(messages.PROMPT_CERT_NAME_O.format(DEFAULT_CERT_NAME_O)) or DEFAULT_CERT_NAME_O

                #validate the input from the User
                if( CERT_NAME_O):
                    break

            except ValueError:
                print(messages.ERROR)



    if(CONFIG 
        and "cert_names" in CONFIG
        and "ou" in CONFIG["cert_names"]
        and CONFIG["cert_names"]["ou"]
    ):
        CERT_NAME_OU = CONFIG["cert_names"]["ou"]
    else:
        while True:
            try:
                CERT_NAME_OU = input(messages.PROMPT_CERT_NAME_OU.format(DEFAULT_CERT_NAME_OU)) or DEFAULT_CERT_NAME_OU

                #validate the input from the User
                if( CERT_NAME_OU):
                    break

            except ValueError:
                print(messages.ERROR)



    if(CONFIG 
        and "cert_names" in CONFIG
        and "st" in CONFIG["cert_names"]
        and CONFIG["cert_names"]["st"]
    ):
        CERT_NAME_ST = CONFIG["cert_names"]["st"]
    else:

        #Prompt for the State
        while True:
            try:
                CERT_NAME_ST = input(messages.PROMPT_CERT_NAME_ST.format(DEFAULT_CERT_NAME_ST)) or DEFAULT_CERT_NAME_ST

                #validate the input from the User
                if( CERT_NAME_ST):
                    break

            except ValueError:
                print(messages.ERROR)



    # #**********************************************************************************
    # #               Installation Versions
    # #**********************************************************************************
    '''
        "etcd" : "v3.4.10"
    '''


    if(CONFIG 
        and "versions" in CONFIG
        and "etcd" in CONFIG["versions"]
        and CONFIG["versions"]["etcd"]
    ):
        ETCD_VERSION = CONFIG["versions"]["etcd"]
    else:
        #Prompt for the Country Name
        while True:
            try:
                ETCD_VERSION = input(messages.PROMPT_ETCD_VERSION.format(DEFAULT_ETCD_VERSION)) or DEFAULT_ETCD_VERSION

                #validate the input from the User
                if( ETCD_VERSION):
                    break

            except ValueError:
                print(messages.ERROR)


    if(CONFIG 
        and "versions" in CONFIG
        and "kubelet" in CONFIG["versions"]
        and CONFIG["versions"]["kubelet"]
    ):
        KUBELET_VERSION = CONFIG["versions"]["kubelet"]
    else:
        #Prompt for the Country Name
        while True:
            try:
                KUBELET_VERSION = input(messages.PROMPT_KUBELET_VERSION.format(DEFAULT_KUBELET_VERSION)) or DEFAULT_KUBELET_VERSION

                #validate the input from the User
                if( KUBELET_VERSION):
                    break

            except ValueError:
                print(messages.ERROR)


    if(CONFIG 
        and "versions" in CONFIG
        and "kubeadm" in CONFIG["versions"]
        and CONFIG["versions"]["kubeadm"]
    ):
        KUBEADM_VERSION = CONFIG["versions"]["kubeadm"]
    else:
        #Prompt for the Country Name
        while True:
            try:
                KUBEADM_VERSION = input(messages.PROMPT_KUBEADM_VERSION.format(DEFAULT_KUBEADM_VERSION)) or DEFAULT_KUBEADM_VERSION

                #validate the input from the User
                if( KUBEADM_VERSION):
                    break

            except ValueError:
                print(messages.ERROR)


    if(CONFIG 
        and "versions" in CONFIG
        and "docker" in CONFIG["versions"]
        and CONFIG["versions"]["docker"]
    ):
        DOCKER_VERSION = CONFIG["versions"]["docker"]
    else:
        #Prompt for the Country Name
        while True:
            try:
                DOCKER_VERSION = input(messages.PROMPT_DOCKER_VERSION.format(DEFAULT_DOCKER_VERSION)) or DEFAULT_DOCKER_VERSION

                #validate the input from the User
                if( DOCKER_VERSION):
                    break

            except ValueError:
                print(messages.ERROR)


    if(CONFIG 
        and "versions" in CONFIG
        and "kubectl" in CONFIG["versions"]
        and CONFIG["versions"]["kubectl"]
    ):
        KUBECTL_VERSION = CONFIG["versions"]["kubectl"]
    else:
        #Prompt for the Country Name
        while True:
            try:
                KUBECTL_VERSION = input(messages.PROMPT_KUBECTL_VERSION.format(DEFAULT_KUBECTL_VERSION)) or DEFAULT_KUBECTL_VERSION

                #validate the input from the User
                if( KUBECTL_VERSION):
                    break

            except ValueError:
                print(messages.ERROR)

    
        
    # #**********************************************************************************
    # #               Firewall rules 
    # #**********************************************************************************

    #Prompt firewall ports tcp and udp

    if(CONFIG 
        and "firewall_ports" in CONFIG
        and "master" in CONFIG["firewall_ports"]
        and "tcp" in CONFIG["firewall_ports"]["master"]
        and CONFIG["firewall_ports"]["master"]["tcp"]
    ):
        FIREWALL_PORTS_TCP = CONFIG["firewall_ports"]["master"]["tcp"]
    else:
        #Prompt for the firewall ports tcp
        while True:
            try:
                FIREWALL_PORTS_TCP = input(messages.PROMPT_FIREWALL_PORTS_TCP)

                #join the ports seperated by comma into list
                FIREWALL_PORTS_TCP = FIREWALL_PORTS_TCP.split(',')

                #validate the input from the User
                if( FIREWALL_PORTS_TCP):
                    break

            except ValueError:
                print(messages.ERROR)

    if(CONFIG 
        and "firewall_ports" in CONFIG
        and "master" in CONFIG["firewall_ports"]
        and "udp" in CONFIG["firewall_ports"]["master"]
        and CONFIG["firewall_ports"]["master"]["udp"]
    ):
        FIREWALL_PORTS_UDP = CONFIG["firewall_ports"]["master"]["udp"]
    else:
        #Prompt for the firewall ports udp
        while True:
            try:
                FIREWALL_PORTS_UDP = input(messages.PROMPT_FIREWALL_PORTS_UDP)

                FIREWALL_PORTS_UDP = FIREWALL_PORTS_UDP.split(',')

                #join the ports seperated by comma into list

                #validate the input from the User
                if( FIREWALL_PORTS_UDP):
                    break

            except ValueError:
                print(messages.ERROR)


    #Prompt firewall ports tcp and udp for the worker nodes

    if(CONFIG 
        and "firewall_ports" in CONFIG
        and "worker" in CONFIG["firewall_ports"]
        and "tcp" in CONFIG["firewall_ports"]["worker"]
        and CONFIG["firewall_ports"]["worker"]["tcp"]
    ):
        FIREWALL_PORTS_TCP_WORKER = CONFIG["firewall_ports"]["worker"]["tcp"]
    else:
        #Prompt for the firewall ports tcp
        while True:
            try:
                FIREWALL_PORTS_TCP_WORKER = input(messages.PROMPT_FIREWALL_PORTS_TCP_WORKER)

                #join the ports seperated by comma into list
                FIREWALL_PORTS_TCP_WORKER = FIREWALL_PORTS_TCP_WORKER.split(',')

                #validate the input from the User
                if( FIREWALL_PORTS_TCP_WORKER):
                    break

            except ValueError:
                print(messages.ERROR)

    if(CONFIG 
        and "firewall_ports" in CONFIG
        and "worker" in CONFIG["firewall_ports"]
        and "udp" in CONFIG["firewall_ports"]["worker"]
        and CONFIG["firewall_ports"]["worker"]["udp"]
    ):
        FIREWALL_PORTS_UDP_WORKER = CONFIG["firewall_ports"]["worker"]["udp"]
    else:
        #Prompt for the firewall ports udp
        while True:
            try:
                FIREWALL_PORTS_UDP_WORKER = input(messages.PROMPT_FIREWALL_PORTS_UDP_WORKER)

                FIREWALL_PORTS_UDP_WORKER = FIREWALL_PORTS_UDP_WORKER.split(',')

                #join the ports seperated by comma into list

                #validate the input from the User
                if( FIREWALL_PORTS_UDP_WORKER):
                    break

            except ValueError:
                print(messages.ERROR)




        
    # #**********************************************************************************
    # #               Generate Config Files for each Master and Worker Nodes
    # #**********************************************************************************

    #Create Config files for each directory


    '''
    Specific to Each node
    Keep Alive config
    '''


    #Create a Directory per each node in the given environment
    config_files_path = CWD + "/" + DEPLOYMENT_ENVIRONMENT



    if(os.path.exists(config_files_path) and os.path.exists(config_files_path + '/deploy_config.json')):

        CURRENT_CONFIG = loadFile(config_files_path + '/deploy_config.json')
        

        #GET delta changes between the configuration
        
        while True:
            try:

                if(getDeltaConfig(CONFIG,CURRENT_CONFIG)):
                #Prompt for the Country Name
                    reply = input(messages.PROMT_CHANGE_IN_CONFIG)
                else:
                    reply = input(messages.PROMT_NO_CHANGE_IN_CONFIG)


                if (reply in NO_RESPONSE):
                    print("Operation Cancelled")
                    sys.exit()

                #validate the input from the User
                if( reply in YES_RESPONSE):
                    
                    #Copy the directory
                    print("...Archiving the directory")
                    shutil.copytree(config_files_path, config_files_path +'_archive_'+ str(timestamp))

                    break

            except ValueError:
                print(messages.ERROR)
        

            

    # :
    elif not os.path.exists(config_files_path):
        os.mkdir( config_files_path )


    #Create Deployment config files for each master

    try:

        print("...Generating config files for each master node")
        #Load the Keepalived-restart script and haproxy and k8 installation script 
        Keepalived_restart = loadFile(CWD + '/config_files/haproxy/keepalived-restart.sh')
        #haproxy_install_script = loadFile(CWD + '/config_files/haproxy/haproxy-install.sh')
        #k8s_install_script = loadFile(CWD + '/config_files/k8s/k8s-install.sh')


        #Generate k8s installation script
        k8s_install_script = generateK8sInstallScript(CWD,ETCD_VERSION,KUBEADM_VERSION,KUBECTL_VERSION,KUBELET_VERSION,DOCKER_VERSION)

        #Generate the HA Proxy installation script File
        haproxy_install_script = generateHAProxyInstallScript(CWD,KUBEAPI_SERVER_PORT,HA_PROXY_PORT,ETCD_PEER_PORT,
                ETCD_CLIENT_PORT,INTERNAL_IP_RANGE,FIREWALL_PORTS_TCP,FIREWALL_PORTS_UDP,CALICO_INTERFACE_ID,EXTERNAL_IP_RANGE)


        #load reload_all_config_files_script file
        reload_all_config_files_script = loadFile(CWD + '/config_files/reload_all_config_files.sh')


        for n in range(NUM_MASTERS):
            master_dir_path = config_files_path + '/master' + str(n + 1)
            k8s_dir = "/k8s"
            haproxy_dir = "/haproxy"
            
            if (not os.path.exists(master_dir_path)):
                os.mkdir(master_dir_path)

            if (not os.path.exists(master_dir_path + k8s_dir )):
                os.mkdir(master_dir_path + k8s_dir )       #Create k8s directory

            if (not os.path.exists(master_dir_path + haproxy_dir )):
                os.mkdir(master_dir_path +  haproxy_dir)       #Create HAPROXY directory


            #Generate the HA Proxy Config File for each node
            haproxy_config_file = generateHAProxyConfigFile(CWD,MASTER_IPS,KUBEAPI_SERVER_PORT,HA_PROXY_PORT,HA_PROXY_MODE)


            #Generate the keep alive Config File for each node
            keepalived_config_file = generateKeepAlivedConfigFile(CWD,MASTER_IPS,MASTER_IPS_PUBLIC,n,VIRTUAL_IP_ADDRESS,NODE_INTERFACE_ID,
                    VIRTUAL_IP_ADDRESS_PUBLIC,NODE_INTERFACE_ID_PUBLIC,KEEPALIVED_ACTIVE_PASSIVE)


            #Generate the ETCD Configuration file for each node
            etcd_config_file = generateETCDConfigFile(CWD,MASTER_IPS,n,ETCD_PEER_PORT,ETCD_CLIENT_PORT,DEFAULT_PROTOCOL)


            #Generate the k8s config file
            k8s_config_file = generateK8sConfigFile(CWD,MASTER_IPS,MASTER_IPS_PUBLIC,n,VIRTUAL_IP_ADDRESS,VIRTUAL_IP_ADDRESS_PUBLIC,ETCD_CLIENT_PORT,DEFAULT_PROTOCOL,NUM_MASTERS,HA_PROXY_PORT,POD_SUBNET_CIDR,KUBERNETES_VERSION,DNS_SERVER)


            #generate the reload kubeadm config file 
            kubeadm_reload_script = generateKubeadmReloadFile(CWD,MASTER_IPS,n)
            

            #write the config files in the corresponding directory
            writeConfigFile(master_dir_path + haproxy_dir,"haproxy.cfg",haproxy_config_file)
            writeConfigFile(master_dir_path + haproxy_dir,"keepalived-restart.sh",Keepalived_restart)
            writeConfigFile(master_dir_path + haproxy_dir,"keepalived.conf",keepalived_config_file)
            writeConfigFile(master_dir_path + k8s_dir,"etcd.service",etcd_config_file)
            writeConfigFile(master_dir_path + k8s_dir,"config.yaml",k8s_config_file)

            #Installation scripts
            writeConfigFile(master_dir_path ,"haproxy_install.sh",haproxy_install_script)
            writeConfigFile(master_dir_path ,"k8s_install.sh",k8s_install_script)
            writeConfigFile(master_dir_path ,"reload_all_config_files.sh",reload_all_config_files_script)
            writeConfigFile(master_dir_path ,"reload_kubeadm.sh",kubeadm_reload_script)
            



            #For the Master1 Upload the deploy script and for the rest of the masters upload the join the cluster script
            if(n == 0): #For Master1
                k8s_deploy_script = loadFile(CWD + '/config_files/k8s/deploy-cluster.sh')
                writeConfigFile(master_dir_path ,"deploy-cluster.sh",k8s_deploy_script)

                #Load the calico Interface file
                calico_config_file = generateCalicoConfigFile(CWD,CALICO_INTERFACE_ID)
                writeConfigFile(master_dir_path + k8s_dir ,"calico.yaml",calico_config_file)

            # else:
            #     #Copy the join the worker node script

            #     join_cluster = generateClusterJoinScript(CWD,VIRTUAL_IP_ADDRESS,HA_PROXY_PORT,"MASTER")
            #     #Installation scripts
            #     writeConfigFile(master_dir_path,"join-cluster.sh",join_cluster)
    except OSError as e:
        print ("Creation of the directory %s failed" % config_files_path,e)
        return 
    else:
        print ("...Successfully created the master directorys and config files" )


    # #**********************************************************************************
    # #               Generate the cert config files
    # #**********************************************************************************

    print("...Generating cert config files")

    try:

        cert_file_path = CWD + "/" + "certs"
        
        #Load the Keepalived-restart script and haproxy and k8 installation script 
        ca_config_file = loadFile(CWD + '/config_files/k8s/certs/ca-config.json')
        certs_install_file = loadFile(CWD + '/config_files/k8s/certs/certs_install.sh')
        

        if not os.path.exists(cert_file_path):
            os.mkdir(cert_file_path)   #Create the Certs directory


        #Generate the ca-csr cert Config File 
        ca_csr_config_file = generateCaCsrConfigFile(CWD,CERT_NAME_C,CERT_NAME_L,CERT_NAME_O,CERT_NAME_OU,CERT_NAME_ST)


        #Generate the kubernetes-csr cert Config File 
        kubernetes_csr_config_file = generateKubernetesCsrConfigFile(CWD,MASTER_IPS,MASTER_IPS_PUBLIC,VIRTUAL_IP_ADDRESS,CERT_NAME_C,CERT_NAME_L,CERT_NAME_O,CERT_NAME_OU,CERT_NAME_ST)

        #write the config files in the corresponding directory
        writeConfigFile(cert_file_path,"ca-config.json",ca_config_file)
        writeConfigFile(cert_file_path,"ca-csr.json",ca_csr_config_file)
        writeConfigFile(cert_file_path,"kubernetes-csr.json",kubernetes_csr_config_file)

        #Installation scripts
        writeConfigFile(cert_file_path,"certs_install.sh",certs_install_file)
        

    except OSError as e:
        print ("Creation of the directory %s failed" % config_files_path,e)
        return 
    else:
        print ("...Successfully created the cert config files")




    # #**********************************************************************************
    # #               Generate the worker Node directory and scripts
    # #**********************************************************************************


    print("...Generating config files for each worker node")

    #Create Deployment config files for each worker
    try:
        worker_file_path = config_files_path + '/worker'

        if not os.path.exists(worker_file_path):
            os.mkdir(worker_file_path)

        #Copy the join the worker node script

        # join_worker_node = generateClusterJoinScript(CWD,VIRTUAL_IP_ADDRESS,HA_PROXY_PORT,"WORKER")
        

        # #Installation scripts
        # writeConfigFile(worker_file_path,"join-worker.sh",join_worker_node)


        #k8s_install_script = loadFile(CWD + '/config_files/k8s/k8s-install_worker.sh')
        k8s_install_script = generateWorkerK8Script(CWD,HA_PROXY_PORT,INTERNAL_IP_RANGE,KUBEAPI_SERVER_PORT,
            KUBEADM_VERSION,KUBECTL_VERSION,KUBELET_VERSION,DOCKER_VERSION,FIREWALL_PORTS_TCP_WORKER,FIREWALL_PORTS_UDP_WORKER)

        writeConfigFile(worker_file_path ,"k8s-install_worker.sh",k8s_install_script)


        #load the reload_kubeadm file
        reload_kubeadm_worker = generateKubeadmReloadWorkerFile(CWD,NODE_INTERFACE_ID)
        writeConfigFile(worker_file_path ,"reload_kubeadm_worker.sh",reload_kubeadm_worker)

    except OSError as e:
        print ("Creation of the directory %s failed" % config_files_path,e)
        return
    else:
        print ("...Successfully created the worker directorys")

    # try:
    #     for n in range(NUM_WORKERS):
    #         os.mkdir(config_files_path + '/worker' + str(n))
    # except OSError as e:
    #     print ("Creation of the directory %s failed" % config_files_path,e)
    #     return
    # else:
    #     print ("...Successfully created the worker directorys")
    writeConfigFile(config_files_path ,"deploy_config.json",json.dumps(CONFIG))
    print("Operation Completed")





if __name__ == '__main__': 
    main(sys.argv[1:])
