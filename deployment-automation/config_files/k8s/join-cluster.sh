# #**********************************************************************************
# #                      JOIN the Cluster
# #**********************************************************************************

# Get the Token Values
#  TOKEN
#  HASH_TOKEN
#  certificate Key


#prompt user to enter the TOKEN value
while true
    do
        read -p "Enter TOKEN Value :" TOKEN
        if [ ! -z "$TOKEN" ]; 
        then
            echo "Error:Please enter valid TOKEN"
        else
            break
        fi
    done



#prompt user to enter the HASH_TOKEN
while true
    do
        read -p "Enter HASH_TOKEN Value :" HASH_TOKEN
        if [ ! -z "$HASH_TOKEN" ]; 
        then
            echo "Error:Please enter valid HASH_TOKEN"
        else
            break
        fi
    done



#Prompt use to enter the certificate key value
while true
    do
        read -p "Enter CERTIFICATE_KEY Value :" CERTIFICATE_KEY
        if [ ! -z "$CERTIFICATE_KEY" ]; 
        then
            echo "Error:Please enter valid CERTIFICATE_KEY"
        else
            break
        fi
    done




kubeadm join <VIP_URL> --token $TOKEN --discovery-token-ca-cert-hash  $HASH_TOKEN --control-plane --certificate-key $CERTIFICATE_KEY
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config


echo "Operation Completed"