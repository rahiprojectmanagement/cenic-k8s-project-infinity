curl -L  https://github.com/cloudflare/cfssl/releases/download/v1.4.1/cfssl_1.4.1_linux_amd64 -o cfssl
  chmod +x cfssl
curl -L https://github.com/cloudflare/cfssl/releases/download/v1.4.1/cfssljson_1.4.1_linux_amd64 -o cfssljson
 chmod +x cfssljson
curl -L https://github.com/cloudflare/cfssl/releases/download/v1.4.1/cfssl-certinfo_1.4.1_linux_amd64 -o     cfssl-certinfo
  chmod +x cfssl-certinfo

sudo mkdir -p /etc/etcd /var/lib/etcd
sudo mv cfssl* /bin/

# mkdir certs
# cd certs/


cfssl gencert -initca ca-csr.json | cfssljson -bare ca
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes kubernetes-csr.json | cfssljson -bare kubernetes

sudo cp ca.pem kubernetes.pem kubernetes-key.pem /etc/etcd

echo "Operation Completed"