# #**********************************************************************************
# #                      Deploy the Cluster
# #**********************************************************************************


echo "Deploying the Cluster"




sudo kubeadm init --config ./k8s/config.yaml --upload-certs
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config


#kubectl apply -f https://docs.projectcalico.org/v3.14/manifests/calico.yaml
kubectl apply -f ./k8s/calico.yaml


echo "Operation Completed"