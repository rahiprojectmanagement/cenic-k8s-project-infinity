#!/bin/bash
# #**********************************************************************************
# #                      k8s Control plane installation
# #**********************************************************************************

# This assumes repos are in-place and just installs and sets up environment for docker and vanilla kubernetes
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF

swapoff -a
sed -i 's/^\(.*swap.*\)$/#\1/' /etc/fstab

modprobe br_netfilter

echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

sudo yum update -y
sudo yum-config-manager     --add-repo     https://download.docker.com/linux/centos/docker-ce.repo 


# sudo yum install docker-ce -y
# sudo yum -y install kubectl kubelet kubeadm --disableexcludes=kubernetes


sudo yum install docker-ce-<DOCKER_VERSION> -y
sudo yum -y install kubectl-<KUBECTL_VERSION> kubelet-<KUBELET_VERSION> kubeadm-<KUBEADM_VERSION> --disableexcludes=kubernetes

# mkdir /etc/docker

# cat > /etc/docker/daemon.json <<EOF
# {
#   "exec-opts": ["native.cgroupdriver=systemd"],
#   "log-driver": "json-file",
#   "log-opts": {
#     "max-size": "100m"
#   },
#   "storage-driver": "overlay2",
#   "storage-opts": [
#     "overlay2.override_kernel_check=true"
#   ]
# }
# EOF

# mkdir -p /etc/systemd/system/docker.service.d

# sudo systemctl enable docker.service
# sudo systemctl start docker.service
# sudo systemctl status docker.service
systemctl daemon-reload
systemctl enable docker
systemctl start docker
systemctl status docker

sudo systemctl enable --now kubelet

# systemctl enable kubelet
# systemctl daemon-reload
# systemctl start kubelet
# systemctl status kubelet


# #**********************************************************************************
# #                                ETCD Installation
# #**********************************************************************************




sudo mkdir -p /etc/etcd /var/lib/etcd


ETCD_VER=<ETCD_VERSION>

# choose either URL
GOOGLE_URL=https://storage.googleapis.com/etcd
GITHUB_URL=https://github.com/etcd-io/etcd/releases/download
DOWNLOAD_URL=${GOOGLE_URL}
curl -L ${DOWNLOAD_URL}/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz -o etcd-${ETCD_VER}-linux-amd64.tar.gz

tar xvzf etcd-${ETCD_VER}-linux-amd64.tar.gz
sudo mv etcd-${ETCD_VER}-linux-amd64/etcd* /bin
 
sudo chmod 700 -R /var/lib/etcd/




#Copy the configuration files to the corresponding directory etcd directory
sudo cp ./k8s/etcd.service  /etc/systemd/system/etcd.service



#Enable and start the etcd service
sudo systemctl daemon-reload
sudo systemctl enable etcd
sudo systemctl start etcd


echo "Operation Completed"