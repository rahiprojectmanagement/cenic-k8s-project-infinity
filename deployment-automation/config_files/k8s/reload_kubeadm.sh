
# /usr/lib/systemd/system/kubelet.service.d/10-kubeadm.conf
#Add "--node-ip=$IP" at the end of the second Environment line, where $IP is the IP of the interface Calico is binding to (service/private IP for CENIC)

sudo sed -i 's|KUBELET_CONFIG_ARGS=--config=/var/lib/kubelet/config.yaml|KUBELET_CONFIG_ARGS=--config=/var/lib/kubelet/config.yaml --node-ip=<NODE_IP>|g' /usr/lib/systemd/system/kubelet.service.d/10-kubeadm.conf


#reload the kubelet service
systemctl daemon-reload
systemctl restart kubelet
