#!/bin/bash
# #**********************************************************************************
# #                      k8s Control plane installation
# #**********************************************************************************

echo "... Configuring Firewall"


sudo firewall-cmd --permanent --new-zone=k8s_internal
sudo firewall-cmd --permanent --zone=k8s_internal --change-interface=service
sudo firewall-cmd --permanent --zone=k8s_internal --add-source=<INTERNAL_IP_RANGE> 
sudo firewall-cmd --permanent --zone=k8s_internal --add-masquerade
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=10250/tcp
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=10255/tcp
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=8472/udp
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=10248/tcp
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=30000-32767/tcp
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=<HA_PROXY_PORT>/tcp
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=<KUBEAPI_SERVER_PORT>/tcp
<FIREWALL_CONFIG>
sudo firewall-cmd --reload


echo "...Configured Firewall"


# This assumes repos are in-place and just installs and sets up environment for docker and vanilla kubernetes
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF

swapoff -a
sed -i 's/^\(.*swap.*\)$/#\1/' /etc/fstab

modprobe br_netfilter

echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

sudo yum update -y
sudo yum install -y yum-utils
sudo yum-config-manager     --add-repo     https://download.docker.com/linux/centos/docker-ce.repo 
# sudo yum install docker-ce -y
# sudo yum -y install kubectl kubelet kubeadm --disableexcludes=kubernetes

sudo yum install docker-ce-<DOCKER_VERSION> -y
sudo yum -y install kubectl-<KUBECTL_VERSION> kubelet-<KUBELET_VERSION> kubeadm-<KUBEADM_VERSION> --disableexcludes=kubernetes

# mkdir /etc/docker

# cat > /etc/docker/daemon.json <<EOF
# {
#   "exec-opts": ["native.cgroupdriver=systemd"],
#   "log-driver": "json-file",
#   "log-opts": {
#     "max-size": "100m"
#   },
#   "storage-driver": "overlay2",
#   "storage-opts": [
#     "overlay2.override_kernel_check=true"
#   ]
# }
# EOF

# mkdir -p /etc/systemd/system/docker.service.d

# sudo systemctl enable docker.service
# sudo systemctl start docker.service
# sudo systemctl status docker.service
systemctl daemon-reload
systemctl enable docker
systemctl start docker
systemctl status docker

sudo systemctl enable --now kubelet

# systemctl enable kubelet
# systemctl daemon-reload
# systemctl start kubelet
# systemctl status kubelet


echo "Operation Completed"