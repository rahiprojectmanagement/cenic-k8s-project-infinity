
#GET the ip address of the node
NODE_IP=$(ip addr show <INTERFACE_ID> | grep 'inet ' | cut -f2 | awk '{ print $2}' | awk '{split($0,a,"/"); print a[1]}')
REPLACE_CONFIG="KUBELET_CONFIG_ARGS=--config=/var/lib/kubelet/config.yaml --node-ip=$NODE_IP"

echo $REPLACE_CONFIG

# # /usr/lib/systemd/system/kubelet.service.d/10-kubeadm.conf
# #Add "--node-ip=$IP" at the end of the second Environment line, where $IP is the IP of the interface Calico is binding to (service/private IP for CENIC)

sudo sed -i "s|KUBELET_CONFIG_ARGS=--config=/var/lib/kubelet/config.yaml|$REPLACE_CONFIG|g" /usr/lib/systemd/system/kubelet.service.d/10-kubeadm.conf


# #reload the kubelet service
systemctl daemon-reload
systemctl restart kubelet



echo "kubelet configuration reloaded"