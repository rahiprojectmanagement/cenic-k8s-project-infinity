# #**********************************************************************************
# #                   Reload Config files for HA proxy and keepalivd
# #**********************************************************************************




#Copy the configuration files to the corresponding paths
sudo cp ./haproxy/keepalived-restart.sh /etc/keepalived/keepalived-restart.sh   #Keepalive restart script
sudo cp ./haproxy/keepalived.conf /etc/keepalived/keepalived.conf         #keep alive config file
sudo cp ./haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg


#Start the KeepAlive Service
sudo systemctl enable keepalived
sudo systemctl start keepalived
sudo systemctl status keepalived


#Start the HA Proxy Service
sudo systemctl enable haproxy
setsebool -P haproxy_connect_any=1
sudo systemctl start haproxy
sudo systemctl status haproxy




# #**********************************************************************************
# #                   Reload Config files for etcd
# #**********************************************************************************




#Copy the configuration files to the corresponding directory etcd directory
sudo cp ./k8s/etcd.service  /etc/systemd/system/etcd.service

#Enable and start the etcd service
sudo systemctl daemon-reload
sudo systemctl enable etcd
sudo systemctl start etcd


echo "Operation Completed"





# #**********************************************************************************
# #                   Reload Config files for etcd
# #**********************************************************************************




#Copy the configuration files to the corresponding directory etcd directory
sudo cp ./k8s/etcd.service  /etc/systemd/system/etcd.service

#Enable and start the etcd service
sudo systemctl daemon-reload
sudo systemctl enable etcd
sudo systemctl start etcd


echo "Operation Completed"





