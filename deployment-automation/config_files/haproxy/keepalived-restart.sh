#!/bin/sh


# restart keepalive if haproxy died
pid=`/bin/pgrep haproxy`
test -z "$pid" && { systemctl restart keepalived &>/dev/null; exit 1; }
exit 0
