echo "... Configuring Firewall"

#Stop the Firewall 
# sudo systemctl stop firewalld
# sudo firewall-cmd --state
# sudo systemctl disable firewalld



# External-facing K8s firewall rules
sudo firewall-cmd --permanent --zone=internal --add-source=<EXTERNAL_IP_RANGE>
sudo firewall-cmd --permanent --zone=internal --add-port=6445/tcp


# Internal-facing K8s firewall rules
sudo firewall-cmd --permanent --new-zone=k8s_internal
sudo firewall-cmd --permanent --zone=k8s_internal --change-interface=<CALICO_INTERFACE_ID>
sudo firewall-cmd --permanent --zone=k8s_internal --add-source=<INTERNAL_IP_RANGE>
sudo firewall-cmd --permanent --zone=k8s_internal --add-masquerade
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=10250/tcp
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=10251/tcp
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=10252/tcp
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=10255/tcp
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=8472/udp
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=<HA_PROXY_PORT>/tcp
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=<KUBEAPI_SERVER_PORT>/tcp
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=<ETCD_CLIENT_PORT>/tcp
# sudo firewall-cmd --permanent --zone=k8s_internal --add-port=<ETCD_PEER_PORT>/tcp
<FIREWALL_CONFIG>
sudo firewall-cmd --reload




echo "...Configured Firewall"

#Install HA Proxy and keepAlive
sudo yum install -y haproxy
sudo yum install -y keepalived
sudo yum install -y yum-utils


#Install the dependencies
sudo yum install -y psmisc





#Copy the configuration files to the corresponding paths
sudo cp ./haproxy/keepalived-restart.sh /etc/keepalived/keepalived-restart.sh   #Keepalive restart script
sudo cp ./haproxy/keepalived.conf /etc/keepalived/keepalived.conf         #keep alive config file
sudo cp ./haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg


#Start the KeepAlive Service
sudo systemctl enable keepalived
sudo systemctl start keepalived
sudo systemctl status keepalived


#Start the HA Proxy Service
sudo systemctl enable haproxy
setsebool -P haproxy_connect_any=1
sudo systemctl start haproxy
sudo systemctl status haproxy

