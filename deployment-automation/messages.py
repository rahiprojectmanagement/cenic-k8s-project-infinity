PROMPT_NO_OF_MASTER_NODES = "Please enter the No of Master Nodes [default={0}]:"
PROMPT_NO_OF_WORKER_NODES = "Please enter the No of Worker Nodes [default={0}]:"

INVALID_NUMBER_INPUT = "Please Enter valid Number:"

PROMPT_DEPLOYMENT_ENVIRONMENT = 'Please enter Deployment Environment:'

PROMPT_MASTER_IP = "Please enter the Private IP Address of the master{0} :"
PROMPT_MASTER_IP_ROOT = "Please enter the Private IP Address of the master{0} (ROOT) :"


PROMPT_MASTER_IP_ROOT_PUBLIC = "Please enter the Public IP Address of the master{0} (ROOT) :"
PROMPT_MASTER_IP_PUBLIC = "Please enter the Public IP Address of the master{0} :"

PROMPT_WORKER_IP = "Please enter the IP Address of the worker{0} :"
PROMPT_VIRTUAL_IP_ADDRESS = "Please enter Private Virtual IP Address:"
PROMPT_VIRTUAL_IP_ADDRESS_PUBLIC = "Please enter Public Virtual IP Address:"
PROMPT_INTERNAL_IP_RANGE = "Please enter Internal IP Range for firewall configuration:"
PROMPT_EXTERNAL_IP_RANGE = "Please enter External IP Range for firewall configuration:"

#Error Messages
INVALID_IP_ADDRESS="Error : Invalid IP Address. Please enter valid IP Address:"
PROMPT_HA_PROXY_MODE = "Please Enter valid HA Proxy mode[default={0}]:"
PROMPT_HA_PROXY_PORT = "Please Enter valid HA Proxy port[default={0}]:"
INVALD_HA_PROXY_MODE = "Invalid HA proxy mode:"


#Etcd promt messages
PROMPT_ETCD_PEER_PORT = "Please Enter valid ETCD port for peer communication [default={0}]:"
PROMPT_ETCD_CLIENT_PORT = "Please Enter valid ETCD port for Client request [default={0}]:"


ERROR = "Error Occured"

PROMPT_POD_SUBNET_CIDR = "Please enter valid CIDR for the POD Subnet:"
PROMPT_KUBEAPI_SERVER_PORT = "Please enter valid KubeAPI Server Port[default={0}]:"

INVALID_CIDR_BLOCK = "Error : Invalid CIDR Block. Please enter an acceptable CIDR."
INVALID_CIDR_BLOCK_CUSTOM = "Error : {}. Please enter an acceptable CIDR."


#Certificate Prompt messages
PROMPT_CERT_NAME_C = "Please enter Country Name(C) for cert generation[default={0}]:"
PROMPT_CERT_NAME_L = "Please enter Location(L) for cert generation[default={0}]:"
PROMPT_CERT_NAME_O = "Please enter Organization (O) for cert generation[default={0}]:"
PROMPT_CERT_NAME_OU = "Please enter Organization Unit(OU) for cert generation[default={0}]:"
PROMPT_CERT_NAME_ST = "Please enter State Name(ST) for cert generation[default={0}]:"
PROMPT_NODE_INTERFACE_ID = "Please enter the Private Interface ID:"
PROMPT_NODE_INTERFACE_ID_PUBLIC = "Please enter the Public Interface ID:"
PROMPT_CALICO_INTERFACE_ID= "Please enter calico bind Interface ID:"
PROMPT_KUBERENETES_VERSION = "Please enter Kubernetes Version:"
PROMPT_DNS_SERVER = "Please enter DNS Server:"
PROMPT_FIREWALL_PORTS_TCP = "Please enter the list of firewall ports for TCP protocol seperated by comma (eg: 443,117):"
PROMPT_FIREWALL_PORTS_UDP = "Please enter the list of firewall ports for UDP protocol seperated by comma (eg: 443,117):"
PROMPT_FIREWALL_PORTS_TCP_WORKER = "Please enter the list of worker node firewall ports for TCP protocol seperated by comma (eg: 443,117):"
PROMPT_FIREWALL_PORTS_UDP_WORKER = "Please enter the list of worker node firewall ports for UDP protocol seperated by comma (eg: 443,117):"
PROMPT_KEEPALIVED_ACTIVE_PASSIVE = "Keepalived Active Passive Configurations(True or False) [default={0}]"

#version messages
PROMPT_ETCD_VERSION = "Please enter etcd version [default={0}]:"
PROMPT_KUBECTL_VERSION = "Please enter kubectl version [default={0}]:"
PROMPT_DOCKER_VERSION = "Please enter docker version [default={0}]:"
PROMPT_KUBEADM_VERSION = "Please enter kubeadm version [default={0}]:"
PROMPT_KUBELET_VERSION = "Please enter kubelet version [default={0}]:"


PROMT_CHANGE_IN_CONFIG = "Change in Configuration observed.Do you want to override configuration? (Enter y/n):"
PROMT_NO_CHANGE_IN_CONFIG = "No Change in Configuration observed.Do you want to override configuration? (Enter y/n):"
PROMPT_INVALID_IP_CONFIG = "INVALID Configuration: No of Private Ips is not equal to Public Ips "